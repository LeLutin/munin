require 'spec_helper'

describe 'munin::client' do
  let :pre_condition do
    'include munin'
  end

  let :default_facts do
    {
      :interfaces => 'eth0',
      :kernel => 'Linux',
      :virtual => 'physical',
      :acpi_available => 'absent',
    }
  end

  shared_examples 'debian-client' do |os, majrelease|
    let :facts do
      {
        :operatingsystem => os,
        :operatingsystemmajrelease => majrelease,
        :osfamily => 'Debian',
        :selinux => 'false',
      }.merge(default_facts)
    end
    it { should contain_package('munin-node') }
    it { should contain_package('iproute2') }
    it { should contain_file('/etc/munin/munin-node.conf') }
    it { should contain_class('munin::client::debian') }
  end

  shared_examples 'redhat-client' do |os, majrelease|
    let :facts do
      {
        :operatingsystem => os,
        :operatingsystemmajrelease => majrelease,
        :osfamily        => 'RedHat',
        :selinux => 'true',
      }.merge(default_facts)
    end
    it { should contain_package('munin-node') }
    it { should contain_file('/etc/munin/munin-node.conf') }
  end

  context 'on debian-like system' do
    it_behaves_like 'debian-client', 'Debian', '9'
    it_behaves_like 'debian-client', 'Debian', '10'
    it_behaves_like 'debian-client', 'Ubuntu', '20'
  end

  context 'on redhat-like system' do
    it_behaves_like 'redhat-client', 'CentOS', '6'
    # not supported yet
    # it_behaves_like 'redhat', 'RedHat', '6'
  end

  context 'gentoo' do
    let :facts do
      {
        :operatingsystem => 'Gentoo',
        :operatingsystemmajrelease => '',
        :osfamily        => 'Gentoo',
        :selinux => 'false',
      }.merge(default_facts)
    end
    it { should contain_package('munin-node') }
    it { should contain_file('/etc/munin/munin-node.conf') }
    it { should contain_class('munin::client::gentoo') }
  end

end
